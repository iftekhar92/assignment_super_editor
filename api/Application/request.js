module.exports = (io) => {

	io.sockets.on('connection', function(server){

		console.log ("Socket io is connected");

		server.on('updateContent', function(reqData){
			require('./modules/RequestHandler.js')(reqData, server);
			});
		server.on('getDataFromDB', function(reqData){
 		 require('./modules/RequestHandler.js')(reqData, server);
 		 });

		// Socket is disconnedt
		server.on('disconnect', function() {
		});

		server.on('close', function () {
		    });
	});
 }
