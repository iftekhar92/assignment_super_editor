## Super Editor Frontend Application

Connection can be pass through Environment variable. The name of environment variables are given below.
Note: Please update /config/index.js file as bellow:

<pre><code>
API_HOST= 'This is the name of the host whare API is running'
API_PORT= 'port of appication whare API is running'
</code></pre>

### introduction
This is a Super Editor Frontend Application developed using Reactjs, socket.io-client.

### How to run
Run following commands
<pre><code>
<ol>
<li>npm install</li>
<li>npm start</li>
</ol>
</code></pre>

### Active URL
http://localhost:4000
http://localhost:4000/assignment

As per requirement IDE data should be async to backend, So Currently onBlur event, I have done.
Please check like: Write something in editor and after writting click outside of Editor.
Doing this, you can check data in collection and also you will see after page refresh.
