## Super Editor Backend Application

Connection can be pass through Environment variable. The name of environment variables are given below.
Note: Please update .env file as bellow:

<pre><code>
HOST= 'This is the name of the host whare app is running'
PORT= 'port of appication '
DB_NAME= 'Database name in mongodb'
</code></pre>

### introduction
This is a Super Editor Backend Application developed using nodejs, socket.io.

### How to run
Run following commands
<pre><code>
<ol>
<li>install mongodb globally</li>
<li>npm install</li>
<li>npm start</li>
</ol>
</code></pre>
