require('dotenv').config();
  const http = require('http'),
  dbObject = require('./server/models').connect(`mongodb://${process.env.HOST}/${process.env.DB_NAME}`),
  server = http.createServer(function(request, response) {
    console.log("Server is ready");
 }),
  io = require('socket.io')(server);

 require('./Application/request')(io);

server.listen(process.env.PORT, function() {
    console.log((new Date()) + ' Server is listening on port *'+process.env.PORT);
});
