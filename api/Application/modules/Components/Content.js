const mongoose = require('mongoose'),
    dateFormat = require('dateformat'),
    Cms = require('mongoose').model('Cms');

class Content {

    constructor() {
    }

    // trigre when update any content
    updateContent(reqData, server) {
        let objOutput = {
            code: 202,
            msg: '',
            data: ''
        }
        let promises = Cms.findOne().select('_id');
        promises.exec(function (err, objResult) {
            if (err) {
                objOutput.msg = JSON.stringify(err);
                server.emit('updateContent', objOutput);
            } else {
                let now = new Date();
                let current_datetime = dateFormat(now, "yyyy-mm-dd");
                if (objResult) {
                    Cms.update({_id: objResult._id}, {
                        $set: {
                            body: reqData.content,
                            updated_on: current_datetime
                        }
                    }, (err, resultSet) => {
                        if (err) {
                            objOutput.msg = JSON.stringify(err);
                            server.emit('updateContent', objOutput);
                        } else {
                            objOutput.msg = "Saved";
                            server.emit('updateContent', objOutput);
                        }
                    });
                } else {
                    const CmsObj = new Cms({
                        body: reqData.content,
                        created_on: current_datetime,
                        updated_on: current_datetime
                    });
                    CmsObj.save((err) => {
                        if (err) {
                            objOutput.msg = JSON.stringify(err);
                            server.emit('updateContent', objOutput);
                        } else {
                            objOutput.msg = "Created";
                            server.emit('updateContent', objOutput);
                        }
                    });
                }
            }
        });
    }

    // Featch data
    getDataFromDB(reqData, server) {
        let objOutput = {
            code: 202,
            msg: '',
            data: ''
        }
        let promises = Cms.findOne().select('body');
        promises.exec(function (err, objResult) {
            if (err) {
                objOutput.msg = JSON.stringify(err);
                server.emit('getDataFromDB', objOutput);
            } else {
                if (objResult) {
                    objOutput.data = objResult.body;
                    server.emit('getDataFromDB', objOutput);
                } else {
                    objOutput.data = '';
                    server.emit('getDataFromDB', objOutput);
                }
            }
        });
    }
}

module.exports = Content;
