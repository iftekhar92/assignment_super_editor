import React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const styles = {
    textEditor: {
        width: 500,
        height: 200,
        align: 'center'
    }
}

export const Center = ({
                           value,
                           handleChange,
                           onBlur
                       }) => {
    return (
        <div style={{textAlign: 'center'}}>
            <div style={styles.textEditor}>
                <ReactQuill
                    theme="snow"
                    value={value}
                    onChange={handleChange}
                    onBlur={onBlur}
                />
            </div>
        </div>
    );
}

export default Center;
