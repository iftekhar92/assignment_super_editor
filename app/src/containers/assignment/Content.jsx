import React from 'react';
import Assignment from '../../components/assignment/Center'
import io from 'socket.io-client'
import config from '../../../config';

let socket = io.connect(`http://${config.API_HOST}:${config.API_PORT}`, {transport: ['websocket'], reconnect: true, forceNew: true})

class Content extends React.Component {

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.state = {text: ''}

        this.getDataFromDB = this.getDataFromDB.bind(this)
        this.getResponseFromDB = this.getResponseFromDB.bind(this)

        this.onBlur = this.onBlur.bind(this);
        this.getResponse = this.getResponse.bind(this);
    }

    componentDidMount() {

        this.getDataFromDB();
        socket.on('getDataFromDB', this.getResponseFromDB);
        socket.on('updateContent', this.getResponse);
    }

    getDataFromDB() {

        let obj = {
            Component: './Components/Content.js',
            reqType: 'getDataFromDB'
        };
        socket.emit('getDataFromDB', obj);
    }

    getResponseFromDB(res) {
        this.setState({
            text: res.data
        });
    }

    handleChange(value) {
        this.setState({text: value})
    }

    onBlur(previousRange, source, editor) {
        let obj = {
            Component: './Components/Content.js',
            reqType: 'updateContent',
            content: this.state.text
        };
        socket.emit('updateContent', obj);
    }

    getResponse(data) {
        console.log("data =>", data);
    }

    render() {
        return (
            <Assignment
                value={this.state.text}
                handleChange={this.handleChange}
                onBlur={this.onBlur}
            />
        )
    }
}

export default Content;
