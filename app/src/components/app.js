import {
    BrowserRouter as Router,
    Link,
    Route,
    Switch,
} from 'react-router-dom'
import React from 'react'

// Container load
import Home from '../containers/home/Content';
import Assignment from '../containers/assignment/Content';

const App = (props) => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/assignment" component={Assignment}/>
                <Route render={() => <h1>Page not found</h1>}/>
            </Switch>
        </Router>
    );
}
export default App;
